﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using System;

public class SetButtonLanguage : MonoBehaviour {

    public string ro;
    public string en;
    private GameObject scripts;

	void Start ()
    {
        scripts = GameObject.Find("Scripts");
        try
        {
            scripts.GetComponent<UIButtons>().addButtonLanguageScript(this);
        }
        catch(Exception e)
        {

        }
        
        changeLanguage();
	}

    public void changeLanguage()
    {
        if (SceneManager.language.Equals("en"))
        {
            transform.GetChild(0).GetComponent<Text>().text = en;
        }
        else if (SceneManager.language.Equals("ro"))
        {
            transform.GetChild(0).GetComponent<Text>().text = ro;
        }
    }
}
