﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using System.IO;
using Client;
using System;

public class DownloadMuseum : MonoBehaviour {

    public bool isAndroidDevice;
    public Text status;

    private Client.Museum museum;
    private List<Client.Exhibit> exhibits;

    private void Start()
    {
        if (!Directory.Exists("/sdcard/SmartMuseum"))
        {
            Directory.CreateDirectory("/sdcard/SmartMuseum");
        }
    }

    public void downloadMuseum(string museumName)
    {
        if (isAndroidDevice)
        {
            PlayerPrefs.SetInt("android", 1);
        }
        else
        {
            PlayerPrefs.SetInt("android", 0);
        }
        PlayerPrefs.Save();

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            status.text = "Please check your internet connection!";
        }
        else
        {
            try
            {
                Client.Client.connectToServer("18.191.129.149", 8081);
                //Client.Client.connectToServer("127.0.0.1", 8001);

                //Client.Client.connectToServer("192.168.0.100", 8001);
                print("Connected");
            }
            catch (Exception e)
            {
                print("Can't connect to server!");
            }

            //Compresser.DecompressZip(SceneManager.path + SceneManager.separator + "TestMuseum.zip", SceneManager.path + SceneManager.separator + "Muzeu de test");

            //museum = new Client.Museum(SceneManager.path + SceneManager.separator + "TestMuseum");
            //museum = new Client.Museum(SceneManager.path + SceneManager.separator + "Muzeu de test");
            status.text = "Downloading...";
            museum = new Client.Museum(Client.Client.GetBinaryWriter(), Client.Client.GetBinaryReader(), museumName);
            //System.IO.File.WriteAllText("/sdcard/SmartMuseum/museum.txt", museumName);
            status.text = "Done!";
        }
    }
}
