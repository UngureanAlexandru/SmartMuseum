﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButtons : MonoBehaviour {

    public GameObject firstPanel;
    public GameObject languagePanel;
    public GameObject loginPanel;
    public GameObject registerPanel;
    public GameObject locationErrorPanel;
    public GameObject downloadPanel;

    private GameObject[] panels;

    public Text responseText;
    public GameObject loginButton;

    private float latitude;
    private float longitude;

    // Temporary data. Real data will come from server
    private float museumLatitude = 100;
    private float museumLongitude = 100;
    private int range = 100;

    List<SetButtonLanguage> setButtonLanguageList;

    private void Awake()
    {
        setButtonLanguageList = new List<SetButtonLanguage>();
        panels = new GameObject[6];

        panels[0] = firstPanel;
        panels[1] = languagePanel;
        panels[2] = loginPanel;
        panels[3] = registerPanel;
        panels[4] = locationErrorPanel;
        panels[5] = downloadPanel;

        string language = PlayerPrefs.GetString("Language");
        
        if (language != "")
        {
            SceneManager.language = language;
        }
        else
        {
            SceneManager.language = "en";
        }
        

        Invoke("checkLocationStatus", 1);
    }

    private void checkLocationStatus()
    {
        int error = GPS.Instance.getErrorCode();

        if (GPS.Instance.getLatitude() > 0 && GPS.Instance.getLongitude() > 0)
        {
            loginButton.GetComponent<Button>().interactable = true;
            responseText.text = "Now you can login!";
        }
        else if (error != 0)
        {
            if (error == 1)
            {
                responseText.text = "You must enable the GPS in order to login!";
                Invoke("checkLocationStatus", 1);
            }

            if (error == 2)
            {
                responseText.text = "I can't get your location!";
            }
        }
        else
        {
            Invoke("checkLocationStatus", 1);
        }
    }

    public void login()
    {
        latitude = GPS.Instance.getLatitude();
        longitude = GPS.Instance.getLongitude();

        float distance = Mathf.Sqrt((museumLatitude - latitude) * (museumLatitude - latitude) + (museumLongitude - longitude) * (museumLongitude - longitude));
        if (distance <= range)
        {
            goToLevel(1);
        }
        else
        {
            responseText.text = "Not in range of a museum!";
            locationErrorPanel.SetActive(true);
        }
    }

    public void createAccount()
    {

    }

    public void goToRegisterPanel()
    {
        loginPanel.SetActive(false);
        registerPanel.SetActive(true);
    }

    public void goToLoginPanel()
    {
        loginPanel.SetActive(true);
        registerPanel.SetActive(false);
    }

    private void goToLevel(int level)
    {
        Application.LoadLevel(level);
    }

    public void setLanguage(string language)
    {
        SceneManager.language = language;
        PlayerPrefs.SetString("Language", language);

        for (int i = 0; i < setButtonLanguageList.Count; i++)
        {
            setButtonLanguageList[i].changeLanguage();
        }
    }

    public void addButtonLanguageScript(SetButtonLanguage script)
    {
        setButtonLanguageList.Add(script);
        print("ok");
    }

    public void goToFirstPanel()
    {
        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].SetActive(false);
        }

        firstPanel.SetActive(true);
    }

    public void goToLanguagePanel()
    {
        firstPanel.SetActive(false);
        languagePanel.SetActive(true);
    }

    public void goToDownloadPanel()
    {
        firstPanel.SetActive(false);
        downloadPanel.SetActive(true);
    }

    public void visitMuseum()
    {
        firstPanel.SetActive(false);
        loginPanel.SetActive(true);
    }
}
